-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2016 at 05:16 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `registration_form`
--

-- --------------------------------------------------------

--
-- Table structure for table `input`
--

CREATE TABLE IF NOT EXISTS `input` (
  `ID` int(11) NOT NULL,
  `First_name` varchar(255) NOT NULL,
  `Last_name` varchar(255) NOT NULL,
  `Email_address` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `Birthday` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Gender` varchar(255) NOT NULL,
  `Images` text NOT NULL,
  `Tearms_condition` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `input`
--

INSERT INTO `input` (`ID`, `First_name`, `Last_name`, `Email_address`, `password`, `Birthday`, `Address`, `Gender`, `Images`, `Tearms_condition`) VALUES
(1, 'resma', 'akter', 'neil@gmail.com', 'resa', '01/jan/1993', 'khulna', 'femae', 'jhhfjhf', 'i agree tearms and condition'),
(2, 'jesma', 'akter', 'jesma@gmail.com', 'jesma', '', 'gdgdfgfdg', 'male', 'P1250002.JPG', 'I accept the Terms and condition'),
(3, 'farida', 'khanm', 'faridakhanam001@gmail.com', 'farida', '', 'goplgong', 'female', '', 'I accept the Terms and condition'),
(4, 'farida', 'khanm', 'faridakhanam001@gmail.com', '', '', 'goplgong', 'female', '', 'I accept the Terms and condition'),
(5, 'resma', 'khanm', 'simu@gmail.com', 'fff', '16/apr/2002', 'vvvv', 'female', '', ''),
(6, 'robi', 'khan', 'jiniashimu@gmail.com', 'sdf', '15/jan/1987', 'fdd', 'male', '', 'I accept the Terms and condition'),
(7, '', '', '', '', '0/Month/0', '', '', '', ''),
(8, '', '', '', '', '0/Month/0', '', '', '', ''),
(9, 'ami', 'apu', 'neilresa@gmail.com', 'sd', '13/apr/2006', 'cfdf', 'female', '', 'I accept the Terms and condition'),
(10, 'ami', 'apu', 'neilresa@gmail.com', '', '13/apr/2006', 'cfdf', 'female', '', 'I accept the Terms and condition'),
(11, 'ami', 'apu', 'neilresa@gmail.com', '', '13/apr/2006', 'cfdf', 'female', '', 'I accept the Terms and condition'),
(12, 'tanu', 'khanm', 'neilresa@gmail.com', 'jbbkj', '17/apr/2004', 'hhv', 'female', '', 'I accept the Terms and condition'),
(13, 'abbu', 'harun', 'neilresa@gmail.com', 'dfd', '14/may/2008', 'zfdfd', 'female', '', 'I accept the Terms and condition'),
(14, 'sabuj', 'khan', 'angeljesma@gmail.com', 'sds', '13/may/2010', 'sfddf', 'male', '', 'I accept the Terms and condition'),
(15, 'resma', 'akter', 'neilresa@gmail.com', 'ssd', '12/apr/2010', '', 'female', 'P1250002.JPG', 'I accept the Terms and condition'),
(16, 'ami', 'khanm', 'neilresa@gmail.com', 'Ssd', '7/jun/2005', 'sfsr', 'female', 'P1250006.JPG', 'I accept the Terms and condition'),
(17, 'neil', 'resa', 'neilresa@gmail.com', 'dfsfd', '11/aug/2005', 'dffddf', 'male', 'P1250009.JPG', 'I accept the Terms and condition');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `input`
--
ALTER TABLE `input`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `input`
--
ALTER TABLE `input`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
